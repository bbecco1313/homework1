#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		float fadeColor(float s);
		float fadeBrightness(float b);

		//Variables
		int radius;        // Width of the shape
		float xPos, yPos;    // Starting position of shape    

		float xSpeed;  // Speed of the shape
		float ySpeed;  // Speed of the shape

		int xDirection;  // Left or Right
		int yDirection;  // Top to Bottom

		float hue; //random color every time you run
		int saturation;
		int brightness;

		float linePosX; //line position
		float linePosY;
		float linePrevX; //remember previous values
		float linePrevY;

		float r;
		float g;
		float b;

		//float hue;
		//float saturation;
		//float brightness;
		
};
