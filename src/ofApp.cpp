#include "ofApp.h"

//Blake Becco Coding Project 1
//Instructions:
//Click the mouse to draw lines randomly that follow the snake
//Press SPACEBAR to change the color of the snake and lines
//Press "S" key to save an image of the output as a JPEG file
//WATCH AND ENJOY!! THE OUTPUT WILL BE DIFFERENT DEPENDING ON HOW YOU INTERACT!! WOO!!

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(90); //fast to keep ellipses attached
	ofSetBackgroundAuto(false);
	//ellipseMode(RADIUS);
	// Set the starting position of the shape to be random
	xPos = ofRandom(ofGetWidth() / 2);
	yPos = ofRandom(ofGetHeight() / 2);
	ofBackground(0);
	//colorMode(HSB, 360, 100, 100); //change color mode to allow color saturation fade

	//Variables
	radius = 25;        // Width of the shape

	xPos = ofRandom(ofGetWidth() / 2); // Starting position of shape  
    yPos = ofRandom(ofGetHeight() /2);      

	xSpeed = 5;  // Speed of the shape
	ySpeed = 3;  // Speed of the shape

	xDirection = 1;  // Left or Right
	yDirection = 1;  // Top to Bottom

	hue = ofRandom(360); //random color every time you run
	saturation = 50;
	brightness = 50;
}

//--------------------------------------------------------------
void ofApp::update(){
	// Update the position of the shape to generate motion
	xPos = xPos + (xSpeed * xDirection);
	yPos = yPos + (ySpeed * yDirection);

	// Test to see if the shape exceeds the boundaries of the screen
	// If it does, reverse its direction by multiplying by -1
	if (xPos > ofGetWidth() - radius || xPos < radius) {
		xDirection *= -1;
	}
	if (yPos > ofGetHeight() - radius || yPos < radius) {
		yDirection *= -1;
	}

	linePosX = xPos + ofRandom(-150, 150); //line position
	linePosY = yPos + ofRandom(-150, 150);;
	linePrevX = linePosX; //remember previous values
	linePrevY = linePosY;
}

//--------------------------------------------------------------
void ofApp::draw(){
	//modified drunk walk
	linePrevX = linePosX; //remember previous
	linePrevY = linePosY; //remember previous
	linePosX = xPos + ofRandom(-150, 150); // update x position to follow ellipses
	linePosY = yPos + ofRandom(-150, 150); // update y position to follow ellipses
	//end modified drunk walk

	// Draw the shape
	//ofColor c = ofColor::fromHsb(hue, fadeColor(saturation), fadeBrightness(brightness), 1);
	ofSetColor(r, g, b);
	ofDrawEllipse(xPos, yPos, radius, radius);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == ' ') {
		//hue = ofRandom(360);
		r = ofRandom(255);
		g = ofRandom(255);
		b =	ofRandom(255);
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	ofSetLineWidth(3);
	ofSetColor(r, g, b);
	ofDrawLine(linePrevX, linePrevY, linePosX, linePosY);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

float ofApp::fadeColor(float s) { //Fades color in and out
	float fader = (ofGetFrameNum() % (int)s) + 40; //fade color saturation, add 40 to keep darker colors
	return fader;
}

float ofApp::fadeBrightness(float b) { //Fades brightness in and out
	float newBrightness = (ofGetFrameNum() % (int)b) + 50; //fade color brightness, add 50 to keep from being black
	return newBrightness;
}
